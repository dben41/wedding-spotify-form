import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormEditorComponent } from './components/form-editor/form-editor.component';
import { FormsListComponent } from './components/forms-list/forms-list.component';


const routes: Routes = [{ path: '', component: FormsListComponent, pathMatch: 'full' },
  { path: 'edit', component: FormEditorComponent },
  { path: 'home', component: FormsListComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
