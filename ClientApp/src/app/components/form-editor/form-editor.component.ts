import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SpotifyPlaylist } from '../../core/models/response/spotifyPlaylist';
import { SpotifyService } from '../../core/services/spotify.service';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-form-editor',
  templateUrl: './form-editor.component.html',
  styleUrls: ['./form-editor.component.css']
})
export class FormEditorComponent implements OnInit {
  ceremonyProcessional: string = "3RV1t6g4VRdUkAVMRZXObk";
  ceremonyProcessionalPlaylist: SpotifyPlaylist;
  constructor(private spotifyService: SpotifyService) { }

  ngOnInit(): void {
    //get playlists
    this.spotifyService.getSpotifyPlaylist(this.ceremonyProcessional).subscribe(response => {
      this.ceremonyProcessionalPlaylist = response;
    });

    console.log(this.ceremonyProcessionalPlaylist);
  }


  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();



}
