import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { SpotifyPlaylist } from '../models/response/spotifyPlaylist';
import { BaseService } from './base.service';

  @Injectable({
  providedIn: 'root'
  })
export class SpotifyService extends BaseService {
  then(arg0: (googleUser: any) => void) {
       throw new Error('Method not implemented.');
    }
  private oauthEndpoint: string = `https://accounts.spotify.com/api/token`;
  private playlistEndpoint: string = 'https://api.spotify.com/v1/playlists';
  private weddingCeremonySuggestions: string = '3RV1t6g4VRdUkAVMRZXObk';
  private clientId: string = '00e6309118a24ce5bc615bede4c2fdf3';
  private clientSecret: string = '593f800233d14afa91f6060eb667f074';

  
    constructor(httpClient: HttpClient) {
        super(httpClient);
    }

    spotifyAuthenticate(): Observable < any > {
      var base64Auth = btoa(`${this.clientId}:${this.clientSecret}`);
      const headers = { 'Authorization': 'Basic ' + base64Auth };
        const body = new HttpParams().set('grant_type', 'client_credentials').set('x-www-form-urlencoded', '');
      return this.httpClient.post<any>(this.oauthEndpoint, body, { headers: new HttpHeaders({ 'Authorization': 'Basic ' + base64Auth, 'Content-Type': 'application/x-www-form-urlencoded' }) });
    }

    getSpotifyPlaylist(playlistUrl: string): Observable<any> {
      return this.spotifyAuthenticate().pipe(flatMap(token => {
        const headers = { 'Authorization': 'Bearer ' + token.access_token };
        return this.httpClient.get<any>(`${this.playlistEndpoint}/${playlistUrl}`, { headers });
      }))
    }
  }
