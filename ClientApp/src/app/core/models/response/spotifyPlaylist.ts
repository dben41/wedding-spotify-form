export interface SpotifyPlaylist {
  tracks: Tracks
}

export interface Tracks {
  track: Track[]
}

export interface Track {
  name: string;
  id: string; //for embed
  artists: Artist[];
}

export interface Artist {
  name: string;
}
